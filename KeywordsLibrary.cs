﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AlgorithmDLL
{
    public class KeywordsLibrary
    {
        private Dictionary<string, List<string>> lib = null;
        private static KeywordsLibrary instance = null;
        private static object lockobj = new object();
        private KeywordsLibrary() 
        {
            lib = new Dictionary<string,List<string>>();
        }

        public static KeywordsLibrary GetInstance()
        {
            if (instance == null)
            {
                lock (lockobj)
                {
                    if (instance == null)
                    {
                        instance = new KeywordsLibrary();
                    }
                }
            }
            return instance;
        }

        public List<string> GetKeywords(string lang)
        {
            if (lib.ContainsKey(lang))
                return lib[lang];
            else
            {
                if (!File.Exists("languages/" + lang + ".txt"))
                    throw new FileNotFoundException("someone is asshole...");
                using (StreamReader guf = new StreamReader("languages/" + lang + ".txt", Encoding.UTF8))
                {
                    List<string> tmp = new List<string>();
                    while (!guf.EndOfStream)
                    {
                        tmp.Add(guf.ReadLine());
                    }
                    lib.Add(lang, tmp);
                    return lib[lang];
                }
            }
        }
    }
}
