﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace AlgorithmDLL
{
    public class CodeToken
    {
        private string lang = string.Empty;
        private string code = string.Empty;

        public CodeToken(string path, string codeLang)
        {
            this.lang = codeLang;
            StreamReader tmpStream = new StreamReader(path, Encoding.UTF8);
            string tmp = string.Empty;
            while (!tmpStream.EndOfStream)
            {
                tmp = tmpStream.ReadLine();
                tmp = Regex.Replace(tmp, @"\/\/.*", " ");
                code += tmp;
            }
            tmpStream.Close();
            code = Regex.Replace(code, @"/\*.*?\*/|\s+", " ");
            foreach (string keyword in KeywordsLibrary.GetInstance().GetKeywords(lang))           
                code = code.Replace(keyword, "k");
            code = Regex.Replace(code, "\".*?\"", "\"i\"");
            code = Regex.Replace(code, @"[A-jl-z][A-z0-9]*", "i");
            code = Regex.Replace(code, @"[0-9]+", "n");
            code = Regex.Replace(code, @"<<|>>", "s");
            code = Regex.Replace(code, @"\s+", "");
        }

        public override string ToString()
        {
            return code;
        }
    }
}
